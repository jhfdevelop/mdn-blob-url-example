import express from 'express'

const mainPort = 4000
const framePort = 4001

const frameSourceMiddleware = (req, res, next) => {
  res.setHeader('Content-Security-Policy', 'frame-src self localhost:4001')
  next()
}

const mainServer = express()
mainServer.use('/', frameSourceMiddleware, express.static('public'))

const frameServer = express()
frameServer.use('/', express.static('frames'))

frameServer.listen(framePort, () => console.log(`frames are being served from localhost:${framePort}`))
mainServer.listen(mainPort, () => console.log(`main web server listening on port ${mainPort}. Open http://localhost:${mainPort}`))