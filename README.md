# mdn-blob-url-example

This project shows a problem with the example code for the [Blob-URL documentation page from mdn](https://developer.mozilla.org/en-US/docs/Web/API/Blob#creating_a_url_representing_the_contents_of_a_typed_array)

See [Github Issue](https://github.com/mdn/content/issues/30049) for the original description of the problem

## Possible Solution?
I think, a solution would be to set a target="_blank" on the link with the blob URL. That could at least work on the main documentation page where the frame is not sandboxed. (Please forgive me if that is not a viable solution and I might be missing something)

## Start the demo
To run the demo, assuming, you have nodeJS installed, clone this repo and run
```shell
npm i
npm run start
```

Then open http://localhost:4000 in the browser. Additional info about what is supposed to happen is in the code and on the served page.